package ca.ualberta.cs.lonelytwitter.lonelytweet;

import android.util.Log;

import java.util.Date;

public class ImportantLonelyTweet extends LonelyTweet {

	// Removed unused constructor

	public ImportantLonelyTweet(String text, Date date) {
		this.tweetDate = date;
		this.tweetBody = text;
	}

	@Override
	public String getTweetBody(){
		Log.i("Important Tweet", tweetBody.toUpperCase());
		return tweetBody.toUpperCase();
	}

}